
import java.util.Scanner;

public class FigurinhasDaCopa {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, c, m, aux, f;

        n = in.nextInt();
        c = in.nextInt();
        f = c;
        m = in.nextInt();

        int x[] = new int[c];
        int y[] = new int[m];

        for (int i = 0; i < c; i++) {
            x[i] = in.nextInt();
        }
        for (int i = 0; i < m; i++) {
            y[i] = in.nextInt();
        }
        
        // sortear as figurinhas que já tenho por ordem crescente
        for (int i = 0; i < m - 1; i++) {
            for (int i2 = 0; i2 < m - 1; i2++) {
                if(y[i2] > y[i2 + 1]){
                    aux = y[i2];
                    y[i2] = y[i2+1];
                    y[i2 + 1] = aux;
                }
            }
        }
        
        // substituir o número de figurinhas repetidas por -1
        for(int i = 0; i < m - 1; i++){
            if(y[i] == y[i+1]){
                y[i] = -1;
            }
        }
        
        // comparar as figurinhas que tenho com as figurinhas carimbadas
        for(int i = 0; i < c; i++){
            for(int i2 = 0; i2 < m; i2++){
                if(x[i] == y[i2]){
                    f--;
                }
            }
        }
        
        System.out.println(f);
    }
}
