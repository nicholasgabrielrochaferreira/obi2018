
import java.util.Scanner;

public class CamaraDeCompensacao {
    // Para ajudar na visualização, caso necessário
    public static void listar(int[] x, int[] v, int[] y){
        System.out.println("-----------------");
        for(int i = 0; i < x.length; i++){
            System.out.println(x[i] + " " + v[i] + " " + y[i]);
        }
    }
    public static void listar2(int[] z){
        System.out.println("-----------------");
        for(int i = 1; i < z.length; i++){
            System.out.println(i + " = " + z[i]);
        }
    }
    
    public static void main(String[] args) {
        // Inicialização e leitura de variáveis
        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        int n = in.nextInt();
        int x[] = new int[m];
        int v[] = new int[m];
        int y[] = new int[m];
        int saldo[] = new int[n+1];
        int melhor = 0;
        int soma = 0;

        for (int i = 0; i < m; i++) {
            // Inicialização e leitura de variáveis
            x[i] = in.nextInt();
            v[i] = in.nextInt();
            y[i] = in.nextInt();
            
            // Calcular o saldo de cada morador
            saldo[y[i]] += v[i];
            saldo[x[i]] -= v[i];
            
            // Calcular o valor total de transações
            soma += v[i];
        }
        
        // Calcular melhor valor de cheques possível
        // melhor = soma dos valores positivos ou negativos dos saldos dos moradores
        // || soma dos valores absolutos dos saldos dos moradores dividido por 2
        for(int i = 0; i < saldo.length; i++){
            if(saldo[i] < 0){
                melhor += -saldo[i];
            } else{
                melhor += saldo[i];
            }
        }
        melhor /= 2;
        
        // Se a soma dos valores dos cheques for igual ao melhor valor de cheques,
        //não há uma maneira melhor de realizar as transações
        if(melhor == soma){
            System.out.println("N");
        } else {
            System.out.println("S");
        }
        System.out.println(melhor); 
    }
}
