
import java.util.Scanner;

public class PisoDaEscola {
    public static void main(String[] args) {
        int l, c, t1, t2;
        Scanner in = new Scanner(System.in);
        
        l = in.nextInt();
        c = in.nextInt();
        
        t1 = l * c  + (l-1) * (c-1);
        t2 = (l-1) * 2 + (c-1) * 2;
        
        System.out.println(t1 + "\n" + t2);
    }
}
